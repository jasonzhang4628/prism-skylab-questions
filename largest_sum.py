"""
I chose Python as the language for this question because the problem is fairly
straightforward and Python provides accessible, built in tools for list
manipulations.

As for my approach, I used a rolling sum to determine when to scrap the
subsequence when it became useless, or essentially, when the subsequence's
sum dipped below zero, it became no better than simply starting anew at 0.

"""


def brute_sum(input_list):
    """
    Brute force solution to longest contiguous sum problem.
    This solution runs in O(n^2) time because the number of possible
    subsequences is calculated by the summation of i, from i = 0 to the
    length of the list.
    >>> a = [1, 2, 3]
    >>> brute_sum(a)
    [1, 2, 3]
    >>> b = [1, -2, 2]
    >>> brute_sum(b)
    [2]
    >>> c = [-2, -1, -3]
    >>> brute_sum(c)
    [-1]
    """
    length = len(input_list)
    best_sum, curr_sum = 0, 0
    subsequence = []
    for i in range(length):
        for j in range(i, length + 1):
            curr_sum = sum(input_list[i: j])
            if curr_sum > best_sum:
                best_sum = curr_sum
                subsequence = input_list[i: j]
    if not best_sum:
        return [max(input_list)]
    return subsequence

def better_sum(input_list):
    """
    Iterates over the list once, checking the current sum. If the sum ever
    dips to zero or below, it indicates that the previous sequence has
    peaked and will grow no further than starting over. The function then
    saves that sequence if its sum is the largest and calculates the sum
    of the next sequence anew.
    The running time of this function is O(n) because it iterates over the
    list once, and it is the absolute minimum running time because you must
    at least check the entire input to calculate this solution, which is
    O(n).
    >>> a = [1, 2, 3]
    >>> better_sum(a)
    [1, 2, 3]
    >>> b = [1, -2, 2]
    >>> better_sum(b)
    [2]
    >>> c = [1, 2, -3]
    >>> better_sum(c)
    [1, 2]
    >>> d = [-2, -1, -3]
    >>> better_sum(d)
    [-1]
    """
    curr_sum, best_sum = 0, 0
    subsequence, best_subsequence = [], []
    for i in range(len(input_list)):
        curr_sum += input_list[i]
        subsequence += [input_list[i]]
        if curr_sum <= 0:
            curr_sum = 0
            subsequence = []
        elif curr_sum > best_sum:
            best_sum = curr_sum
            best_subsequence = subsequence[:]
    if not best_subsequence:
        return [max(input_list)]
    return best_subsequence

