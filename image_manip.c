/* Methods for image manipulation library. 
   See image_manip.txt for more short answers.
*/

#include <stdlib.h>
#include <stdio.h>
#include "image_manip.h"

/* Uncertain about reading from a file. Assuming it succeeds, elements are written
   fields in structure. */
Image create(int x, int y, char color, char format, char* file);

/* Fetches width. */
int width(Image image) {
    return image->x;
}

/* Fetches height. */
int height(Image image) {
    return image->y;
}

/* Image copy via creation of new structure. */
Image copy(Image image) {
    Image image_copy = malloc(sizeof(Image));
    if (image_copy == NULL):
        // failed alloc, handle
    image_copy->x = image->x;
    image_copy->y = image->y;
    image_copy->color = image->color;
    image_copy->format = image->format;
    if (image->color):
        image_copy->color = 3;
    int size = image_copy->color * image_copy->x * image_copy->y;
    int *contents = malloc(size * sizeof(int));
    for (int i = 0; i < size; i++) {
        contents[i] = image->contents[i];
    }
    return image_copy;
}

/* Sets value at coordinate x, y to array value (in case colors need to be set). */
void set(int x, int y, int *value, Image image) {
    if (x > image->x || y > image->y) {
        return;
    }
    int step = 1;
    if (image->color) {
        step = 3;
    }
    for (int i = 0; i < sizeof(value) / sizeof(int); i++) {
        image->contents[x * y * step + i] = value[i];
    }
}

/* Returns array of values at location x,y based on whether image is colored. */
int *get(int x, int y, Image image) {
    int size = 1;
    int *value = int[1];
    if (image->color) {
        value = int[3];
        size = 3;
    }
    for (int i = 0; i < sizeof(value) / sizeof(int); i++) {
        value[i] = image->contents[x * y * size + i];
    }
    return value;
}

/* I had to google the definition of convolve and kernel as it relates to image
   processing. Does that fall under the definition of syntactical with respect
   to the English language? ehhh.... answer is hand-wavey regardless, so feel free
   to disregard it.

   Run two loops to overlay the kernel over the array in every possible position,
   taking care not to overstep the bounds. For each overlay, perform gets to acquire
   the numbers for the calculation, then perform sets to save the new values into the
   image.
   */
Image convolve(Image image, Image kernel);

/* I imagine this requires calculating the projection of the matrix using vector
   transformations but the exact math escapes me at the moment. */
Image scale(Image image, int factor);

