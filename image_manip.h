#include <stdlib.h>
#include <stdio.h>

/* An image structure consisting of ints for dimensions, one bit for color, one
   bit for the number representation, and a pointer to the appropriate data type
   (a little fuzzy on the actual pointer type syntax, but I'm assuming we can
   reinterpret the values as floats if need be). */
typedef struct Image {
    int x;
    int y;
    char color;
    char format;
    int *elems //uncertain pointer type
} Image;

Image create(int x, int y, char color, char format, char* file);

/* Fetches width. */
int width(Image image);

/* Fetches height. */
int height(Image image);

/* Image copy via creation of new structure. */
Image copy(Image image);

/* Sets value at coordinate x, y to array value (in case colors need to be set). */
void set(int x, int y, int *value, Image image);

/* Returns array of values at location x,y based on whether image is colored. */
int *get(int x, int y, Image image);

/* I had to google the definition of convolve and kernel as it relates to image
   processing. Does that fall under the definition of syntactical with respect
   to the English language? ehhh.... answer is hand-wavey regardless, so feel free
   to disregard it.

   Run two loops to overlay the kernel over the array in every possible position,
   taking care not to overstep the bounds. For each overlay, perform gets to acquire
   the numbers for the calculation, then perform sets to save the new values into the
   image.
   */
Image convolve(Image image, Image kernel);

/* I imagine this requires calculating the projection of the matrix using vector
   transformations but the exact math escapes me at the moment. */
Image scale(Image image, int factor);
